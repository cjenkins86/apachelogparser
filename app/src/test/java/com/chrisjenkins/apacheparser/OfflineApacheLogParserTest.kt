package com.chrisjenkins.apacheparser

import android.content.res.AssetManager
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Test
import java.io.ByteArrayInputStream
import org.junit.Assert.*

class OfflineApacheLogParserTest {

    @Test
    fun parseSingleLineLogWithZeroResults () {
        var testData = "123.4.5.9 - - [03/Sep/2013:18:34:48 -0600] \"GET /team/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0"
        val inputStream = ByteArrayInputStream(testData.toByteArray())
        val assetManager = mock<AssetManager> {
            on { open("Apache.log") } doReturn inputStream
        }

        val parser = OfflineApacheLogParser(assetManager)

        val log = parser.parse("Apache.log")

        assertEquals(ApacheLog(arrayOf()), log)
    }

    @Test
    fun parseMultipleLinesWithOneResult () {
        var testData =
                    "123.4.5.9 - - [03/Sep/2013:18:34:48 -0600] \"GET /products/car/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0\n" +
                    "123.4.5.9 - - [03/Sep/2013:18:35:58 -0600] \"GET /products/car/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0\"\n" +
                    "123.4.5.9 - - [03/Sep/2013:18:36:58 -0600] \"GET /products/car/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0\""
        val inputStream = ByteArrayInputStream(testData.toByteArray())
        val assetManager = mock<AssetManager> {
            on { open("Apache.log") } doReturn inputStream
        }

        val parser = OfflineApacheLogParser(assetManager)

        val log = parser.parse("Apache.log")

        assertEquals(ApacheLog(arrayOf(ApacheLogResult("123.4.5.9", "/products/car/", 1))), log)
    }

    @Test
    fun parseLogWithMultipleResults () {
        var testData =
                    "123.4.5.9 - - [03/Sep/2013:18:34:48 -0600] \"GET /products/car/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0\n" +
                    "123.4.5.9 - - [03/Sep/2013:18:35:58 -0600] \"GET /products/car/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0\"\n" +
                    "123.4.5.9 - - [03/Sep/2013:18:36:58 -0600] \"GET /products/car/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0\"\n" +
                    "123.4.5.2 - - [03/Sep/2013:18:35:48 -0600] \"GET /access/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"+
                    "123.4.5.8 - - [03/Sep/2013:18:36:18 -0600] \"GET /contact/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"+
                    "123.4.5.8 - - [03/Sep/2013:18:36:18 -0600] \"GET /contact/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"+
                    "123.4.5.8 - - [03/Sep/2013:18:36:18 -0600] \"GET /contact/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"+
                    "123.4.5.8 - - [03/Sep/2013:18:36:18 -0600] \"GET /contact/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"+
                    "123.4.5.8 - - [03/Sep/2013:18:36:18 -0600] \"GET /contact/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"+
                    "123.4.5.8 - - [03/Sep/2013:18:36:18 -0600] \"GET /contact/ HTTP/1.1\" 200 3327 \"-\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36\"\n"


        val inputStream = ByteArrayInputStream(testData.toByteArray())
        val assetManager = mock<AssetManager> {
            on { open("Apache.log") } doReturn inputStream
        }

        val parser = OfflineApacheLogParser(assetManager)

        val log = parser.parse("Apache.log")

        assertEquals(
            ApacheLog(arrayOf(
                ApacheLogResult("123.4.5.9", "/products/car/", 1),
                ApacheLogResult("123.4.5.8", "/contact/", 2))),
            log)
    }

}