package com.chrisjenkins.apacheparser

import com.nhaarman.mockitokotlin2.*
import org.junit.Test

class LogListPresenterTest {
    @Test
    fun displayItemsDescending() {
        val log = ApacheLog(arrayOf(
            ApacheLogResult("1.0.0.1", "/contacts", 1),
            ApacheLogResult("1.0.0.2", "/user", 4),
            ApacheLogResult("1.0.0.3", "/cars", 2),
            ApacheLogResult("1.0.0.4", "/payments", 3)
        ))

        val logParser = mock<ApacheLogParser> {
            on { parse("Apache.log") } doReturn (log)
        }

        val fakeIdGenerator = mock<IdGenerator>{
            on { gen() } doReturn "undef"
        }

        val presenter = LogListPresenter(fakeIdGenerator, logParser)
        val view = mock<LogListContract.View>()
        val items = arrayOf(
            LogListItem("undef", "1.0.0.2", "/user", 4),
            LogListItem("undef", "1.0.0.4", "/payments", 3),
            LogListItem("undef", "1.0.0.3", "/cars", 2),
            LogListItem("undef", "1.0.0.1", "/contacts", 1)
        )

        presenter.start(view)

        verify(view).displayLogItems(argWhere { it.contentEquals(items) })
        verify(fakeIdGenerator, times(4)).gen()
    }
}