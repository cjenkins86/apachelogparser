package com.chrisjenkins.apacheparser

data class LogListItem (val id:String, val user:String, val sequence:String, val frequency:Int)