package com.chrisjenkins.apacheparser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.log_list_item.view.*

class LogListItemAdapter (var dataset:Array<String> = arrayOf())
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_HEADER = 1
        const val TYPE_CELL = 2
    }

    class CellViewHolder (v : View) : RecyclerView.ViewHolder(v) {
        var value: TextView = v.value
    }

    class HeaderViewHolder (v : View) : RecyclerView.ViewHolder(v) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == TYPE_HEADER) {
            return HeaderViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.log_list_header, parent, false))
        }

        return CellViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.log_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position != 0) {
            (holder as CellViewHolder).value.text = dataset[position - 1]
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return TYPE_HEADER
        }
        return TYPE_CELL
    }

    override fun getItemCount(): Int = dataset.size
}