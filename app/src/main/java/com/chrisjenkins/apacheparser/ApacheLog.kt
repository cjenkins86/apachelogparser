package com.chrisjenkins.apacheparser

data class ApacheLogResult (val user:String, val route:String, val count:Int)

data class ApacheLog (val results:Array<ApacheLogResult>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ApacheLog

        if (!results.contentEquals(other.results)) return false

        return true
    }

    override fun hashCode(): Int {
        return results.contentHashCode()
    }
}