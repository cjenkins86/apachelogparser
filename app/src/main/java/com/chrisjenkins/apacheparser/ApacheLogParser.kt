package com.chrisjenkins.apacheparser

interface ApacheLogParser {
    fun parse (name:String) : ApacheLog
}