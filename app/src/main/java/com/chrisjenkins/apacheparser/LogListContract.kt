package com.chrisjenkins.apacheparser

class LogListContract {
    interface Presenter {
        fun start (view: View)
        fun destroy ()
    }
    interface View {
        fun displayLogItems (items: Array<LogListItem>)
    }
}