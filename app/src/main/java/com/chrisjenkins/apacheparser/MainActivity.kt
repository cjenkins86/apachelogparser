package com.chrisjenkins.apacheparser

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), LogListContract.View {

    private lateinit var presenter: LogListContract.Presenter
    private lateinit var adapter: LogListItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        adapter = LogListItemAdapter()

        val layoutManager = GridLayoutManager(this, 3)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapter.getItemViewType(position)) {
                    LogListItemAdapter.TYPE_HEADER -> 3
                    LogListItemAdapter.TYPE_CELL -> 1
                    else -> -1
                }
            }
        }

        itemsRecyclerView.layoutManager = layoutManager
        itemsRecyclerView.adapter = adapter

        presenter = LogListPresenter(IdGenerator(), OfflineApacheLogParser(assets))
        presenter.start(this)
    }

    override fun displayLogItems (items: Array<LogListItem>) {
        adapter.dataset = items.fold(mutableListOf<String>(), { cells, next -> foldIntoCells(next, cells) })
            .toTypedArray()
        adapter.notifyDataSetChanged()
    }

    private fun foldIntoCells (item: LogListItem, cells:MutableList<String>) : MutableList<String> {
        cells.addAll(mutableListOf(item.user, item.sequence, item.frequency.toString()))
        return cells
    }
}
