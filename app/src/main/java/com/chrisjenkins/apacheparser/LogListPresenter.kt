package com.chrisjenkins.apacheparser

class LogListPresenter (
    private val idGenerator: IdGenerator,
    private val logParser: ApacheLogParser) : LogListContract.Presenter {

    override fun start (view:LogListContract.View) {
        val items = logParser.parse("Apache.log").results.map {
            LogListItem(idGenerator.gen(), it.user, it.route, it.count)
        }.sortedByDescending { it.frequency }.toTypedArray()

        view.displayLogItems(items)
    }

    override fun destroy () {

    }
}