package com.chrisjenkins.apacheparser

import android.content.res.AssetManager
import java.io.IOException
import java.io.InputStreamReader
import java.util.regex.Pattern

class UserLog (val user:String, var curPath:String = "", var curSeqCount:Int = 0,
               var sequences:HashMap<String, Int> = hashMapOf())

class OfflineApacheLogParser (private val assets: AssetManager) : ApacheLogParser {

    override fun parse (filename:String) : ApacheLog {
        var reader : InputStreamReader? = null
        try {
            reader = InputStreamReader(assets.open(filename))

            var logText = reader.readText()

            val regex = "^(\\S+) (\\S+) (\\S+) " +
                    "\\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(\\S+)" +
                    " (\\S+)\\s*(\\S+)?\\s*\""

            val pattern = Pattern.compile(regex, Pattern.MULTILINE)
            val matcher = pattern.matcher(logText)

            val users = hashMapOf<String, UserLog>()

            while (matcher.find()) {
                val ip = matcher.group(1)
                val path = matcher.group(6)

                if (users.containsKey(ip)) {
                    var user = users[ip]!!
                    if (user.curPath != "" && user.curPath == path) {
                        user.curSeqCount++
                        if (user.curSeqCount == 3) {
                            if (user.sequences.containsKey(path)) {
                                user.sequences[path] = user.sequences[path]!!.plus(1)
                            } else {
                                user.sequences[path] = 1
                            }
                            user.curPath = ""
                            user.curSeqCount = 0
                        }
                    } else {
                        user.curPath = path
                        user.curSeqCount = 1
                    }
                } else {
                    users[ip] = UserLog(ip, path, 1)
                }
            }

            val results = users.values.flatMap {
                userLog -> userLog.sequences.map { seq -> ApacheLogResult(userLog.user, seq.key, seq.value) }
            }.toTypedArray()

            return ApacheLog(results)

        } catch (io : IOException) {

        } finally {
            reader?.close()
        }

        return ApacheLog(arrayOf())
    }
}