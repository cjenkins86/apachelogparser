package com.chrisjenkins.apacheparser

import java.util.*

class IdGenerator {
    fun gen () : String {
        return UUID.randomUUID().toString()
    }
}